var mongoose = require('mongoose');
const uniqueValidator = require ('mongoose-unique-validator');
var Reserva = require ('./reserva');
const bcryp = require('bcrypt');

var crypto = require('crypto');
var Token = require('../models/token');
var mailer = require('../mailer/mailer');

const saltRounds= 10;
var Schema = mongoose.Schema;


const validateEmail= function(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

var usuarioSchema = new Schema ({
    nombre: {
        type: String,
        trim: true,
        required : [true,"El nombre es obligatorio"]
    },
    email: {
        type:String,
        trim: true,
        required: [true, "El email es obligatorio"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Porfavor ingrese un email valido"],
        match:[/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    
    password: {
        type: String,
        required: [true, "El password es obligatorio"]
    },
    passwordResetToken: String,
    passwordresetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message:'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save',function(next){
    if (this,this.isModified('password')){
        this.password = bcryp.hashSync(this.password,saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword= function(password){
    return bcryp.compareSync(password, this.password);
}

usuarioSchema.methods.reservar= function(biciId, desde , hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb)
};

usuarioSchema.statics.allUsuarios = function(cb) {
    return this.find({ }, cb);
};

usuarioSchema.statics.add = function(user, cb) {
    return this.create(user, cb);
};

usuarioSchema.statics.findByName = function(name, cb) {
    return this.findOne({nombre: name }, cb);
};

usuarioSchema.statics.removeByName = function(name, cb) {
    return this.deleteOne({nombre: name}, cb);
}



usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const email_destination = this.email;
    
    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n' + 'Por favor, para verificar su cuenta haga click en el siguiente link: \n\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) {
                return console.log(err.message);
            }

            console.log('Se ha enviado un email de bienvenida a ' + email_destination + '.');
        });
    });
};

module.exports = mongoose.model('Usuario', usuarioSchema);